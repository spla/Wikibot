# wikibot
Wikipedia bot for Mastodon

Your users can query your wikibot anything they like. wikibot will reply with Wikipedia answers to them! 

### wikibot usage

  @yourbot query: any_string_here  
  @yourbot info

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon server (admin)

### Usage:

Within Python Virtual Environment:

1. Run `python install -r requirements.txt` to install needed Python libraries.

2. Run `python db-setup.py` to create needed database and tables. All queries data will be written there. You can use software like Grafana to visualize your wikibot metrics!

3. Run `python setup.py` to get your Mastodon's access token of an existing user. It will be saved to 'secrets/secrets.txt' for further use.

4. Run `python wikibot.py` to reply any queries done by users with: @yourbot query: any_string_here 

5. Use your favourite scheduling method to set wikibot.py to run regularly.

### New feature!

Now wikibot replies Stats Information with: @yourbot info  
16.11.19 - Added set Wikipedia lang (saved to lang_config.txt file). Set the language with one of two letters prefixes according with the [List of all Wikipedias](http://meta.wikimedia.org/wiki/List_of_Wikipedias)  
23.10.20 - Recoded wikibot.py to avoid using Mastodon's API. Now it gets all needed data from Mastodon's database.  
23.10.20 - setup.py now ask for Wikipedia & bot language. Only 'ca' and 'en' actually supported.
